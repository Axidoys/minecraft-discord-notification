# Analysis for the advancement notification

In order to add the notification of the advancement, I had to check how to retrieve the icon/name.

In "advancement_raw" you have the files from the Minecraft 1.19.3, just the "recipes" has been remove.

Checked if all the advancements from are shown in chat or other properties (spoiler alert : almost yes):

```shell
cat $(find . -name '*.json') | jq -s 'flatten' | jq '.[] | {parent, atc: .display.announce_to_chat}'
```

# Retrieve icons and text

Actually I havent found alive API and it was hard to generate the data from the minecraft sources. Just for the icon, you have to find 2 png (bg and item), and for the item it depends on the nbt tag...

So I have done some scraping on : https://minecraft.fandom.com/wiki/Advancement .

You can check the generator icon_scraping.py and the result in scrapped_icons/ .
