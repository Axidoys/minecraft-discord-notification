import re
from time import sleep

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def get_row_infos(row):
    cols = row.find_elements(By.XPATH, ".//td")

    name = cols[1].text
    pruned_name = re.sub(r'[^\w\s]', '', name).lower().replace(" ", "_")  # For file naming, URL, ...
    description = cols[2].text

    advancement_icon = row.find_elements(By.CSS_SELECTOR, ".plainlinks")[0]

    # take a screenshot of the wrapping div containing the advancement image
    screenshot_filename = f'advancement_{pruned_name}.png'
    advancement_icon.screenshot("./scrapped_icons/" + screenshot_filename)

    # save description
    with open(f"./scrapped_icons/advancement_{pruned_name}.txt", "w") as file:
        file.write(description)

    return {"name": name, "pruned_name": pruned_name, "description": description}


# create a new Chrome browser instance
browser = webdriver.Chrome()

# navigate to the Advancement page on the Minecraft Wiki
browser.get('https://minecraft.fandom.com/wiki/Advancement')

# Wait for the page to load
wait = WebDriverWait(browser, 20)
wait.until(EC.presence_of_element_located((By.CLASS_NAME, "plainlinks")))
sleep(1)

# Remove scrollbar for further screenshots
style = """
    ::-webkit-scrollbar {
        width: 0px;
        background: transparent; /* make scrollbar transparent */
    }
"""
browser.execute_script(
    "var style = document.createElement('style'); style.innerHTML = %r; document.head.appendChild(style);" % style)

# Hide the banner
banner = browser.find_element(By.CSS_SELECTOR, 'div[data-tracking-opt-in-overlay="true"]')
browser.execute_script("arguments[0].setAttribute('style', 'display:none;')", banner)

# Hide notif
notif = browser.find_element(By.CSS_SELECTOR, '.notifications-placeholder')
browser.execute_script("arguments[0].setAttribute('style', 'display:none;')", notif)

# find the first advancement listed on the page
advancement_divs = browser.find_elements(By.CSS_SELECTOR, '.wikitable[data-description="advancements"]')

for table in advancement_divs:

    table_content = table.find_element(By.XPATH, ".//tbody")

    for row in table_content.find_elements(By.XPATH, ".//tr"):

        try:
            # In order to force the loading
            actions = ActionChains(browser)
            actions.move_to_element(row).perform()
            wait.until(EC.visibility_of(row))

            # Screenshot and more info if needed
            print(get_row_infos(row))
        except Exception as e:
            print("Error while parsing row (", row.text[:30], ") :", e)

# close the browser
browser.quit()
