import cgi
import json
import sys
from datetime import datetime
from random import randint
from http.server import BaseHTTPRequestHandler, HTTPServer

filename = "./mock-logs.log"

host_name = "localhost"
server_port = 8080

options = [
    "[Server thread/INFO]: Axel joined the game",
    "[Server thread/INFO]: Julien joined the game",
    "[Server thread/INFO]: [Not Secure] <Axel> Je vais dans l'église, je me sens en sécurité",
    "[Server thread/INFO]: Axel was slain by Spider",
    """[Server thread/INFO]: Olaf joined the game
[Server thread/INFO]: Pierre was slain by Spider
[Server thread/INFO]: Olaf left the game""",
    """[Server thread/INFO]: Pierre was slain by Spider
[Server thread/INFO]: Pierre was slain by Spider""",
    "[Server thread/INFO]: Olaf has made the advancement [Eye Spy]",
    "[Server thread/INFO]: Olaf has made the advancement [Fishy Business]",
    "[Server thread/INFO]: Axel has made the advancement [Is It a Bird?]",
    "[Server thread/INFO]: Axel has made the advancement [zDoes not exist]",
    "[Server thread/INFO]: Axel has completed the challenge [zIt Spreads]"
]

expectations = [
    "Axel s'est connecté",
    "Julien s'est connecté",
    "",
    "",
    """Olaf s'est connecté
Olaf s'est déconnecté""",
    "",
    "Olaf a fini un nouvel avancement",
    "Olaf a fini un nouvel avancement",
    "Axel a fini un nouvel avancement",
    "Axel a fini [zDoes not exist]",
    "Axel a fini [zIt Spreads]"
]

stub_result = []


class MyServer(BaseHTTPRequestHandler):

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        ctype, pdict = cgi.parse_header(self.headers.get('Content-type'))

        # refuse to receive non-json content
        if ctype != 'application/json':
            self.send_response(400)
            self.end_headers()
            return

        # read the message and convert it into a python dictionary
        length = int(self.headers.get('Content-length'))
        message = json.loads(self.rfile.read(length))

        global stub_result
        stub_result += [message['content']]

        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write(bytes("Good good", "utf-8"))


# return something like : [HH:mm:ss]
def time_tag() -> str:
    return datetime.now().strftime("[%H:%M:%S]")


if __name__ == '__main__':
    # decide what to do
    option_index = 0
    if len(sys.argv) < 2:
        option_index = randint(0, 5)
    else:
        option_index = int(sys.argv[1])

    decided_new_logs = options[option_index]
    print("Nouveau mock : ", decided_new_logs)

    # do the mock actions
    log_file = open(filename, "a")

    log_file.write(
        "\n" +
        "\n".join(
            [time_tag() + " " + line
             for line in decided_new_logs.splitlines()]
        )
    )

    log_file.close()

    # expect to receive calls on http://127.0.0.1:8080 if appropriate
    expected = expectations[option_index]

    web_server = HTTPServer((host_name, server_port), MyServer)
    web_server.timeout = 60  # put 5 when the MVP will be running, 60 for testing manually&locally

    if expected == "":
        web_server.handle_request()
        assert len(stub_result) == 0, "should not have any requests !"
    else:
        # for each expected web hook trigger
        for expected_trigger in expected.splitlines():
            web_server.handle_request()

            last_result = stub_result.pop(0)
            assert len(stub_result) == 0  # so we can assume always have a singleton list
            print(f"Got 1 web-hook trigger : [{last_result}]")

            assert expected_trigger.rstrip('\n') == last_result

    print("Successsss !!!")
