import json
import re
import urllib.parse
from datetime import datetime

import requests as requests
import sys

HTTP_HEADERS = {'Content-type': 'application/json'}
RESSOURCE_BRANCH = "master"


# RESSOURCE_BRANCH = "avancement" # For testing !

def get_last_known_line():
    try:
        with open("last_known_line.txt", "r") as file:
            return file.readline()
    except FileNotFoundError:
        return None


def save_last_known_line(last_known_line):
    with open("last_known_line.txt", "w") as filout:
        filout.write(last_known_line)


def find_new_lines(lines, last_known_line):
    new_lines = []
    a = 0
    for k in range(0, len(lines)):
        if lines[k] == last_known_line:
            a = k + 1
    for i in range(a, len(lines)):
        new_lines.append(lines[i])
    return new_lines


def advancement_discord_notif(username, advancement):
    advancement_pruned = re.sub(r'[^\w\s]', '', advancement).lower().replace(" ", "_")

    url = f"https://gitlab.com/Axidoys/minecraft-discord-notification/-/raw/{RESSOURCE_BRANCH}/advancements/scrapped_icons/advancement_{advancement_pruned}.txt"
    response = requests.get(url)
    if not response.ok: raise Exception("Bad advancement name or ressource is not online")

    description = response.content.decode()

    return {
        "content": f"{username} a fini un nouvel avancement",
        "embeds": [
            {
                "title": f"{advancement}",
                "description": description,
                "url": "https://minecraft.fandom.com/wiki/Advancement#List_of_advancements",
                "color": 5810431,
                "thumbnail": {
                    "url": f"https://gitlab.com/Axidoys/minecraft-discord-notification/-/raw/{RESSOURCE_BRANCH}/advancements/scrapped_icons/advancement_{advancement_pruned}.png"
                }
            }
        ]
    }


if __name__ == '__main__':
    print("Hello woooorld ! ", datetime.now().strftime("[%H:%M:%S]"))

    # read arguments : <log-file-path> <discord-hook-url>
    log_file_path = sys.argv[1]
    discord_hook_url = sys.argv[2]

    # read log files
    log_file = open(log_file_path, "r")
    lines = [line.rstrip('\n') for line in log_file.readlines() if line.strip() != '']

    # case of the first run ? don't do any action and prepare last_known_line file
    last_known_line = get_last_known_line()
    if last_known_line is None:
        save_last_known_line(lines[-1])
        exit(0)

    # retrieve only the new lines from the log-file :
    new_lines = find_new_lines(lines, last_known_line)
    save_last_known_line(lines[-1])
    print(new_lines)

    # on the new lines, check if some correspond to someone is connected/disconnected
    for line in new_lines:
        finished_advancement = None  # None if not a line for advancement
        if 'has completed the challenge' in line: finished_advancement = 'has completed the challenge'
        if 'has made the advancement' in line: finished_advancement = 'has made the advancement'

        # when this is the case, do an HTTP request for Discord
        if ('joined the game' in line) and ("<" not in line):
            x = line.split(" joined")
            y = x[0].split("[Server thread/INFO]: ")
            username = y[1]
            data = {"content": username + " s'est connecté"}
            requests.post(discord_hook_url, data=json.dumps(data), headers=HTTP_HEADERS)
        elif ('left the game' in line) and ("<" not in line):
            x = line.split(" left")
            y = x[0].split("[Server thread/INFO]: ")
            username = y[1]
            data = {"content": username + " s'est déconnecté"}
            requests.post(discord_hook_url, data=json.dumps(data), headers=HTTP_HEADERS)
        elif finished_advancement and ("<" not in line):
            x = line.split(f" {finished_advancement} [")
            y = x[0].split("[Server thread/INFO]: ")
            username = y[1]
            advancement = x[1].rstrip(']')

            try:
                data = advancement_discord_notif(username, advancement)
            except Exception as e:
                print("error: " + e.__str__())
                data = {"content": username + f" a fini [{advancement}]"}
            requests.post(discord_hook_url, data=json.dumps(data), headers=HTTP_HEADERS)
