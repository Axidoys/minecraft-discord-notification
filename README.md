# Minecraft Discord Notification

## Why

I was using [itzg/docker-minecraft-server](https://github.com/itzg/docker-minecraft-server) to manage a Minecraft server.

* When people are not connected to the Minecraft server, others would have liked to know what's the activity on the server
* Also, it is a good point to have "logs" available

While there are other ways to do that : 
* <https://www.spigotmc.org/resources/discordsrv.18494/>
* <https://www.spigotmc.org/resources/discord-integration-allow-minecraft-and-discord-users-to-talk-show-activity-on-discord-1-8-1-19.91088/ >
* ...

The ease of using a native server and the autopause feature make those solution not very appropriate. As developers this is also a learn/play opportunity :) .

## How

Because of the autopause feature, the rcon access is not an option. Then the script will use the log file to detect the events.

Also, the script will be executed with cron, every minute.

## Set it up

You need to set up a webhook on the desired Discord channel, you will retrieve an url.

Then the script will run with Cron : 

```
* * * * .... minecraft-discord-notifications.py <log-file-path> <discord-hook-url>
```

## Developer

You can use mock-logs.py to test the script without a real discord server running.

You can (not mandatory) specify an integer to chose between which test to run.

It will generate fake logs. Those have those shapes :

```
[15:57:34] [Server thread/INFO]: [Not Secure] <Axel> Je vais dans l'église, je me sens en sécurité
[15:57:42] [Server thread/INFO]: Pierre was slain by Spider
[15:57:47] [Server thread/INFO]: [Not Secure] <Axel> oh :o
[15:58:00] [Server thread/INFO]: [Not Secure] <Pierre> j'ai ouvert un menu sans savoir la touche...
[15:58:38] [Server thread/INFO]: [Not Secure] <Axel> Bon je vous laisse, amusez-vous bien, hâte d'être à Jeudi pro 20h si vous êtes chauds !
[15:58:40] [Server thread/INFO]: Axel lost connection: Disconnected
[15:58:40] [Server thread/INFO]: Axel left the game
[16:21:04] [Server thread/INFO]: Julien[/92.169.20.138:4266] logged in with entity id 5401 at (-115.10400195252737, 62.0, 81.7841676368031)
[16:21:04] [Server thread/INFO]: Julien joined the game
```

The script will run HTTP server, where it will expect POST requests like :

```json
{
  "content": "Axel est connecté",
  "embeds": null,
  "username": "Capitaine Croche-ed",
  "attachments": []
}
```

If you see "success" from the output of the script, it is working as expected !
